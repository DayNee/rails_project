# JSON Api blog

## Test task

### Requirements

Використані версії: Ruby 2.5+, RoR 5+, PostgreSQL 10+

### Description

Api is implemented on **Ruby 2.5.3**

Language: English

Rspec tests cover all main code and logic

Api Documentation is generated (```http://localhost:3000/docs```)

### To Start

* Clone this repository
* Install all necessary gems
```
$ Bundle install
```
* Ceate DataBase and migrations
```
$ rails db:create
$ rails db:migrate
```
* Fill Data Base
```
$ rails db:seeds
```
* Start server
```
$ bundle exec rails s
```
* Start tests
```
$ bundle exec rspec
```
* Generate the docs
```  
$ rake docs:generate
```
* Open the docs
```
$ open doc/api/index.html