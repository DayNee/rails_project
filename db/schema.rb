ActiveRecord::Schema.define(version: 2019_06_09_134134) do

  enable_extension "plpgsql"

  create_table "posts", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.integer "rate_total", default: 0
    t.integer "rate_count", default: 0
    t.float "rating", default: 0.0
    t.bigint "user_ip_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id", "user_ip_id"], name: "index_posts_on_user_id_and_user_ip_id"
    t.index ["user_id"], name: "index_posts_on_user_id"
    t.index ["user_ip_id", "user_id"], name: "index_posts_on_user_ip_id_and_user_id"
    t.index ["user_ip_id"], name: "index_posts_on_user_ip_id"
  end

  create_table "user_ips", force: :cascade do |t|
    t.string "ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ip"], name: "index_user_ips_on_ip", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "posts", "user_ips"
  add_foreign_key "posts", "users"
end
